clear; close all
%
%---------------------------Input parameters------------------------------%
%
N   = 1000;     % Number of experiments
J   = 10;       % Number of interactions
n   = 5;        %number of groups
%
%-------------------------------------------------------------------------%
% Build transition probability table
p = [0.0769     0.4615      0           0.4615      0       ;   ...
     0.0526     0.3158      0.1579      0.3158      0.1579  ;   ...
     0          0.4         0.2         0.4         0       ;   ...
     0.0526     0.3158      0.1579      0.3158      0.1579  ;   ...
     0          0.4         0           0.4         0.2     ];

% Preallocate arrays
state_seq   = zeros(J+1,1);
P           = zeros(N,J);
s           = zeros(J+1,1);

% State 1: initial state before interactions;
for i = 1:J
    state_seq(i) = 1;
end

% Probability of first interation is 100%
P(1,1) = 1000;

% Loop over interactions and experiments
for i = 1:N
    for j = 1:Jl
        %
        t = rand;
        group = 1;
        trans = p(state_seq(j),group);    % transition probablility
        %
        while(group <= trans)
            if state_seq(j+1)==group
                break
            else
                group = group + 1;
                trans = trans + p(state_seq(j),group);
            end
        end
        %
        P(state_seq(j+1),j+1) = P(state_seq(j+1),j+1) + 1;
        %
    end
end

% Divide the probability curve by the number of experiments
P = P/1000;

% Compute entropy over interactons (k = 1)
for j = 1:J+1
    for i = 1:n
        if P(i,j) > 0
            s(j) = s(j) - P(i,j)*log(P(i,j));
        end
    end
end



%% Plotting sequence 
t = 0:1:J;

figure('Name','Probability over time')
hold on
plot(t,P(1),'x-')
plot(t,P(2),'o-')
plot(t,P(3),'^-')
plot(t,P(4),'-')
plot(t,P(5),'--')
hold off
xlabel('Interaction number'); ylabel('P_k');


figure('Name','Entropy over time')
plot(t,s,'o-k')
xlabel('Interaction number'); ylabel('Entropy');



