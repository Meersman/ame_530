% p3 hw8
clear; close all

global sigma
sigma = 5.670400e-8;

h = 1.2;
w = 2;
H = h/w;

% Configuration factors
F_11 = 0;
F_12 = sqrt(1+H^2) - H;
F_13 = (1 - F_12)/2;
F_14 = F_13;
F_21 = F_12;
F_22 = 0;
F_23 = F_13;
F_24 = F_14;

T1 = 1690;
T2 = 1000;
T3 = 300;
T4 = 300;
Te = 300;

lam1 = 4.5;

lam1T2 = lam1*T2;

F_lam1T1 = 0.83907;
F_lam1T2 = 0.54878 + (0.57926 - 0.54878)/2;
F_lam2T1 = 1 - F_lam1T1;
F_lam2T2 = 1 - F_lam1T2;


%q2 = sigma*T2^4*((F_lam1T2-0)/(1/0.2+1/0.2+1) + (1-F_lam1T2)/(1/0.65+1/0.65+1));

e1 = 0.2;   E1 = (1-e1)/e1;
e2 = 00.65; E2 = (1-e2)/e2;



a1 = [1/e1     -F_12*E1;
      -F_21*E1 1/e1    ];
a2 = [1/e2     -F_12*E2;
      -F_21*E2 1/e2    ];

C1 = [F_lam1T1*sigma*T1^4 - F_12*F_lam1T2*sigma*T2^4 - sigma*Te^4*(F_13+F_14);
      F_lam1T2*sigma*T2^4 - F_21*F_lam1T1*sigma*T1^4 - sigma*Te^4*(F_23+F_24)];
C2 = [F_lam2T1*sigma*T1^4 - F_12*F_lam2T2*sigma*T2^4 - sigma*Te^4*(F_13+F_14);
      F_lam2T2*sigma*T2^4 - F_21*F_lam2T1*sigma*T1^4 - sigma*Te^4*(F_23+F_24)];


q1 = a1\C1;
q2 = a2\C2;

q = q1+q2;

sol_q1 = -8893.0;
sol_q2 = 110e3;


table(T1,q(1),q(2),sol_q2)


