% p1 hw8
clear; close all

global sigma
sigma = 5.670400e-8;

% Configuation factors
F_s1 = 1/6;
F_s2 = 1/6;
F_s3 = 1/6;
F_s4 = 1/6;
F_s5 = 1/6;
F_s6 = 1/6;
F_ss = 0;

F_11 = 0;
F_12 = 1/2 - sqrt(2)/pi*atan(1/sqrt(2)) + 1/(4*pi)*log(3/4);
F_13 = 1/pi*log(4/3) + 4*sqrt(2)/pi*atan(1/sqrt(2)) - 1;
F_14 = F_12;
F_15 = F_12;
F_16 = F_12;
F_1s = 0;

F_21 = F_12;
F_22 = 0;
F_23 = F_12;
F_24 = F_13;
F_25 = F_12;
F_26 = F_12;
F_2s = F_1s;

F_31 = F_13;
F_32 = F_12;
F_33 = 0;
F_34 = F_12;
F_35 = F_12;
F_36 = F_12;
F_3s = F_1s;

F_41 = F_12;
F_42 = F_13;
F_43 = F_12;
F_44 = 0;
F_45 = F_12;
F_46 = F_12;
F_4s = F_1s;

F_51 = F_12;
F_52 = F_12;
F_53 = F_12;
F_54 = F_12;
F_55 = 0;
F_56 = F_13;
F_5s = F_1s;

F_61 = F_12;
F_62 = F_12;
F_63 = F_12;
F_64 = F_12;
F_65 = F_13;
F_66 = 0;
F_6s = F_1s;

F = [F_11 F_12 F_13 F_14 F_15 F_16 F_1s;
     F_21 F_22 F_23 F_24 F_25 F_26 F_2s;
     F_31 F_32 F_33 F_34 F_35 F_36 F_3s;
     F_41 F_42 F_43 F_44 F_45 F_46 F_4s;
     F_51 F_52 F_53 F_54 F_55 F_56 F_5s;
     F_61 F_62 F_63 F_64 F_65 F_66 F_6s;
     F_s1 F_s2 F_s3 F_s4 F_s5 F_s6 F_ss];

q1 = 103747;
q2 = -4309;
q3 = -9551;
q4 = -35609;
q5 = -16928;
q6 = -37351;
qs = 52331;
sol = [q1 q2 q3 q4 q5 q6 qs]';

T1 = 1200;  e1 = 1.0;   E1 = (1-e1)/e1;
T2 =  800;  e2 = 0.5;   E2 = (1-e2)/e2;
T3 =  700;  e3 = 0.5;   E3 = (1-e3)/e3;
T4 =  400;  e4 = 1.0;   E4 = (1-e4)/e4;
T5 =  200;  e5 = 0.5;   E5 = (1-e5)/e5;
T6 =    0;  e6 = 1.0;   E6 = (1-e6)/e6;
Ts = 1300;  es = 0.4;   Es = (1-es)/es;


% Matrix system of equations [a]*{q} = [C]
% {q} = {q1 q2 q3 q4 q5 q6 qs}'
% right hand side

a = [ 1/e1    -F_12*E2 -F_13*E3 -F_14*E4 -F_15*E5 -F_16*E6 -F_1s*Es;
     -F_21*E1  1/e2    -F_23*E3 -F_24*E4 -F_25*E5 -F_26*E6 -F_2s*Es;
     -F_31*E1 -F_32*E2  1/e3    -F_34*E4 -F_35*E5 -F_36*E6 -F_3s*Es;
     -F_41*E1 -F_42*E2 -F_43*E3  1/e4    -F_45*E5 -F_46*E6 -F_4s*Es;
     -F_51*E1 -F_52*E2 -F_53*E3 -F_54*E4  1/e5    -F_56*E6 -F_5s*Es;
     -F_61*E1 -F_62*E2 -F_63*E3 -F_64*E4 -F_65*E5  1/e6    -F_6s*Es;
     -F_s1*E1 -F_s2*E2 -F_s3*E3 -F_s4*E4 -F_s5*E5 -F_s6*E6  1/es   ];

C = [sigma*(      T1^4 - F_12*T2^4 - F_13*T3^4 - F_14*T4^4 - F_15*T5^4 - F_16*T6^4 - F_1s*Ts^4);
     sigma*(-F_21*T1^4 +      T2^4 - F_23*T3^4 - F_24*T4^4 - F_25*T5^4 - F_26*T6^4 - F_2s*Ts^4);
     sigma*(-F_31*T1^4 - F_32*T2^4 +      T3^4 - F_34*T4^4 - F_35*T5^4 - F_36*T6^4 - F_3s*Ts^4);
     sigma*(-F_41*T1^4 - F_42*T2^4 - F_43*T3^4 +      T4^4 - F_45*T5^4 - F_46*T6^4 - F_4s*Ts^4);
     sigma*(-F_51*T1^4 - F_52*T2^4 - F_53*T3^4 - F_54*T4^4 +      T5^4 - F_56*T6^4 - F_5s*Ts^4);
     sigma*(-F_61*T1^4 - F_62*T2^4 - F_63*T3^4 - F_64*T4^4 - F_65*T5^4 +      T6^4 - F_6s*Ts^4);
     sigma*(-F_s1*T1^4 - F_s2*T2^4 - F_s3*T3^4 - F_s4*T4^4 - F_s5*T5^4 - F_s6*T6^4 +      Ts^4)];


q = a\C;

table(sol,q)
