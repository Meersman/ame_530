% Homework 2, Problem 1
%
%
clear all; close all
%
%
%-------------------------------Inputs------------------------------------%
%
T1 = 300;               % State 1 temperature (k)
P1 = 15;                % State 1 pressrure (bar)
T2 = 500;               % State 2 temperature (k)
R = 289;                % Specific gas constant (kJ/kg K)
gamma = 1.4;            % Specific heat ratio
%
%-------------------------------Calculations-------------------------------%
%
% State 1 Ideal Gas Law
v1 = R*T1/P1;

P2 = P1;                % Constant Pressure Heating
v2 = R*T2/P2;

v3 = v2;
T3 = T1;
P3 = R*T3/v3;

% Plot cycle
% 1-2
v_12 = linspace(v1,v2);
for i = 1:length(v_12)
    p_12(i) = P1;
end

% 2-3
p_23 = linspace(P2,P3);
for i = 1:length(p_23)
    v_23(i) = v2;
end

% 3-1
v_31  = linspace(v1,v3);
p_31 = R*T3*v_31;


plot(v_12,p_12,v_23,p_23,v_31,p_31)
legend('1-2','2-3','3-1')
xlabel('v (m^3/kg)'); ylabel('P (bar)');


P = linspace(P1,P2);
v = linspace(v1,v2);


for i = 1:length(v)
    for j = 1:length(P)
        T(i,j) = P(j)*v(i)/R;
    end
end



%surf(T)