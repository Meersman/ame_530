%% Problem 2-16

Q = 1353; 
sigma = 5.670400e-8;


alpha = (0.9*1.5 + 0.1*98.3)/99.8;

T = (Q/alpha/sigma)^(1/4);

%% Problem 5-16
A1 = 0.7;
A2 = 0.8;

A_dgh = sqrt(0.3^2 + 0.4^2) + sqrt(0.1^2 + 0.4^2);
A_acfe = sqrt(0.1^2 + 0.2^2) + sqrt(0.2^2 + 0.2^2) + sqrt(0.1^2 + 0.3^2);
A_acd = sqrt(0.1^2 + 0.2^2) + sqrt(0.2^2 + 0.3^2);
A_hgfe = sqrt(0.1^2 + 0.4^2)  + 0.2 + sqrt(0.1^2 + 0.3^2);
f_12 = 1/2*(A_dgh + A_acfe - A_acd - A_hgfe);

A_agfe = sqrt(0.1^2 + 0.4^2)  + 0.2 + sqrt(0.1^2 + 0.3^2);
A_dfh = sqrt(0.1^2 + 0.4^2) + sqrt(0.1^2 + 0.4^2);
A_gfd = sqrt(0.1^2 + 0.4^2)  + 0.2 + sqrt(0.1^2 + 0.4^2);
A_eh = sqrt(0.1^2 + 0.4^2);
ff_12 = 1/2*(A_agfe + A_dfh - A_gfd - A_eh);

F_12 = (f_12 + ff_12)/A1;
F_21 = A1/A2*F_12;

A_bfe = sqrt(0.2^2 + 0.4^2) + sqrt(0.1^2 + 0.3^2);
A_cd = sqrt(0.2^2 + 0.3^2);
A_bd = 0.3;
A_cfe = sqrt(0.2^2 + 0.2^2) + sqrt(0.1^2 + 0.3^2);
F_13 = (A_bfe + A_cd - A_bd - A_cfe)/(2*A1);