%% Problem 1-6
%
% Given parameters
%
T = 1200;                       % K
lambda = 4;                     % micrometer
C1 = 0.59552138e8;              % W um^4/(m^2 sr)
C2 = 14387.752;                 % um K
%
% Blackbody spectral intensity
i_lam_b = 2*C1/(lambda^5*(exp(C2/lambda/T) - 1));
%
% Blackbody directional spectral emissive power
theta = 45*pi/180;              % deg to rad
e_lam_b = i_lam_b*cos(theta);   
%
% Wavelength of max intensity
lambda_max = 2897.8/T;
%
% Max intensity
i_lam_b_max = 2*C1/(lambda_max^5*(exp(C2/lambda_max/T) - 1));
%
% Hemispherical total emissive power
sigma = 5.670400e-8;
e_b = sigma*T^4;

%% Problem 1-9
lambda1 = 1.5;
lambda2 = 4;
T = 1300;
i_lam_b1 = 2*C1/(lambda1^5*(exp(C2/lambda1/T) - 1));
i_lam_b2 = 2*C1/(lambda2^5*(exp(C2/lambda2/T) - 1));

ratio = i_lam_b1/i_lam_b2;




%% Problem 1-16
clear; close all

theta1 = atan(3/1.5);
theta2 = atan(3/1);
frac1 = sin(theta2)^2 - sin(theta1)^2;
frac2 = sin(pi/2)^2 - sin(theta2)^2;

%
%load T_1600_k.dat;
load T_2200_k.dat;
load T_2800_k.dat;

semilogx(T_2800_k(:,1),T_2800_k(:,2),'k-^'); hold on
%semilogx(T_2200_k(:,1),T_2200_k(:,2),'k-s'); hold off
%semilogx(T_1600_k(:,1),T_1600_k(:,2),'k-o')

for i = 2:length(T_2800_k)
    dydx(i) = (T_2800_k(i,2) - T_2800_k(i-1,2))/(T_2800_k(i,1) - T_2800_k(i-1,1));
end
lambda = T_2800_k(:,1);

%semilogx(lambda,dydx)
vec = linspace(log(2.59995e-06),log(1))';

lambda = exp(vec);

e_lambda = 5.896582602e-07./lambda;

p1 = semilogx(lambda,e_lambda,'k-o');
lgd = legend('2800 K','Rayleigh-Jean (extended)');

ln_thick = 0.8;
mrk_size = 5;

p1.LineWidth = ln_thick; p1.MarkerSize = mrk_size;
xlabel('Wavelength, $\lambda \ (\mu m)$','Interpreter','latex')
ylabel('Hemispherical spectral emissivity, $\epsilon_\lambda$','Interpreter','latex')

lgd.Interpreter = 'latex';

grid on
%grid minor

ax = gca;
ax.FontName = 'Times';
ax.FontSize = 18;
ax.TitleFontSizeMultiplier = 1.2;
ax.LabelFontSizeMultiplier = 1.0;
ax.GridColor = 'black';
ax.GridAlpha = 0.8;
ax.MinorGridAlpha = 0.2;
ax.Box = 'on';
ax.TickDir = 'in';
%ax.XTick = [0:0.5:1.5];
%ax.YTick = [0:1:6];
ax.XMinorTick = 'on';
ax.YMinorTick = 'on';
%ax.XRuler.MinorTickValues = [0.25:0.5:1.25];
%ax.YRuler.MinorTickValues = [0.5:1:5.5];
ax.TickLength = [0.01 0.01];
ax.LineWidth = 0.8;
ax.XLim = [0 1];
ax.YLim = [0 0.5];
%
lgd.Location = 'NorthEast';
lgd.FontSize = ax.FontSize;

% Combine data
lambda = [T_2800_k(:,1); lambda];
e_lambda_e = [T_2800_k(:,2); e_lambda];

total = 0;
for i = 1:length(lambda)-1
    total = total + 0.5*(lambda(i+1)-lambda(i))*(e_lambda_e(i+1) + e_lambda_e(i));
end

visible = 0;
for i = 9:15
    visible = visible + 0.5*(lambda(i+1)-lambda(i))*(e_lambda_e(i+1) + e_lambda_e(i));
end

efficiency = visible/total;

