%% Problem 1
clear all

L = 2.5;
R = L/2;
A1 = pi*R^2;
A2 = pi*R^2;
A3 = 2*pi*L*R;

R1 = R/L;
R2 = R/L;
X = 1 + (1 + R2^2)/R1^2;
F_12 = 1/2*(X - sqrt(X^2 - 4*(R2/R1)^2));
F_32 = 1/4*(1 - F_12);


T1 = 900;
T2 = 0;
T3 = 900;
eps1 = 0.7;
eps3 = 0.7;

sigma = 5.670400e-8;


Q_12 = sigma*(T1^4 - T2^4)/((1 - eps1)/(eps1*A1) + 1/(F_12*A1));
Q_32 = sigma*(T3^4 - T2^4)/((1 - eps3)/(eps3*A3) + 1/(F_32*A3));

Q_net = Q_12 + Q_32;

eps = Q_net/(pi*R^2*sigma*(T1^4 - T2^4));

%% Problem 2
clear
T_sun = 5780;
S_sun = 1.48e11;
r_sun = 6.95e8;
dA_sun = pi*r_sun^2;
sigma = 5.670400e-8;

% Incoming irradiation from the sun

G_sun = sigma*T_sun^4/(pi*S_sun^2)*dA_sun;

r_disk = 2;
dA_disk = pi*r_disk^2;

%T_disk = (dA_sun*G_sun/dA_disk/sigma)^(1/4)
T_disk = (G_sun/2/sigma)^(1/4);


h = 0.5;
A1 = pi*r_disk^2;
A2 = pi*r_disk^2;
A3 = 2*pi*r_disk*h;
q = G_sun;

% calculate view factor between disk and sheild
R1 = r_disk/h;
R2 = r_disk/h;
X = 1 + (1 + R2^2)/R1^2;
F_12 = 1/2*(X - sqrt(X^2 - 4*(R2/R1)^2));
F_13 = 1 - F_12;
F_21 = A1/A2*F_12;
F_23 = F_13;
F_31 = A1/A3*F_13;
F_32 = A2/A3*F_23;

% Emissivities
e1 = 0.05;
e2 = 0.5;
e3 = 1;

% Build system of equations

%       A(1,1)  A(1,2)  A(1,3)      J1      =   B1
%       A(2,1)  A(2,2)  A(2,3)      J2      =   B2
%       A(3,1)  A(3,2)  A(3,3)      J3      =   B3


A = zeros(3,3);
B = zeros(3,1);

% A(1,1) = - e1*A1/(1 - e1) - F_12*A1 - F_13*A1;
% A(1,2) = F_12*A1;
% A(1,3) = F_13*A1;
% B(1,1) = -q*e1*A1;
% A(2,1) = F_21*A2;
% A(2,2) = - e2*A2/(1 - e2) - F_21*A2 - F_23*A2;
% A(3,3) = F_23*A2;
% B(2,1) = 


A = [-(e1/(1-e1)+1)  F_12            0               ;
     F_21            -(e2/(1-e2)+1)  sigma*e2/(1-e2) ;
     F_31            F_32            0               ];
B = [-q*e1/(1-e1); 0; 0];


sol = inv(A)*B;

T2 = sol(3,1)








%% Problem 3





%% Problem 4