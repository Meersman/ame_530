%% Problem 1
L = 2.5;
R = L/2;
A1 = pi*R^2;
A2 = pi*R^2 + 2*pi*L*R;

F_21 = A1/A2;

T1 = 0;
T2 = 900;
eps2 = 0.7;

sigma = 5.670400e-8;


Q_net_21 = sigma*(T2^4 - T1^4)/((1 + eps2)/(eps2*A2) - 1/(F_21*A2));


%% Problem 2

T_sun = 5780;
S_sun = 1.48e11;
r_sun = 6.95e8;
dA_sun = pi*r_sun^2;

% Incoming irradiation from the sun

G_sun = sigma*T_sun^4/(pi*S_sun^2)*dA_sun;

sigma = 5.670400e-8;

r_disk = 2;
dA_disk = pi*r_disk^2;

%T_disk = (dA_sun*G_sun/dA_disk/sigma)^(1/4)
T_disk = (G_sun/2/sigma)^(1/4);


% calculate view factor between disk and sheild
h = 0.5;
R1 = r_disk/h;
R2 = r_disk/h;
X = 1 + (1 + R2^2)/R1^2;
F_23 = 1/2*(X - sqrt(X^2 - 4*(R2/R1)^2));





%% Problem 3





%% Problem 4